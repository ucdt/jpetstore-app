# jpetstore-app

This repository holds the source code for jpetstore-app 1.0. jpetstore-app
builds the JPetStore WAR file without static content and its database settings
tokenized for MySQL.